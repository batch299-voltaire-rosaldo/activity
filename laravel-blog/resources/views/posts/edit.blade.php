@extends('layouts.app')

@section('content')
    <form action="/posts/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="{{$post->title}}">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" id="content" rows="3">{{$post->content}}</textarea>
            </div>
            
            <div class="mt-2">
                <button class="btn btn-primary" type="submit">Update</button>
            </div>
    </form>
@endsection

