@extends('layouts.app')

@section('content')
    <div class="text-center">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel logo" width="300">
        <h2>Featured Posts:</h2>
    </div>
    
    @if(count($posts) > 0)
        @php
            $maxIterations = 4;
            $counter = 0;
        @endphp
    
        @foreach($posts as $post)
            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                    {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in --}}
                </div>   
            </div>
            @php 
                $counter++;
            @endphp
            @if($counter >= $maxIterations)
                @break
            @endif
        @endforeach
    @endif
@endsection