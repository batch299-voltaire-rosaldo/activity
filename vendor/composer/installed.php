<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.11.0',
      'version' => '0.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ad82ce168c82ba30d1c01ec86116ab52f589478',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '612a3ee5ab0d5dd97b7cf3874a6efe24325efac3',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.6',
      'version' => '2.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9d313a36c872fd6ee06d9a6cbcf713eaa40f024',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39ab8fcf5a51ce4b85ca97c7a7d033eb12831124',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5997fa97e8790cdae03a9cbd5e78e45e3c7bda7',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c719a19c3d8c34b2494a7206f8ffde3eff3f983',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '0168d0e44ea0c4fe5451fe08cde7049b9e9f9741',
    ),
    'illuminate/conditionable' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bea24daa0fa84b7e7b0d5b84f62c71b7e2dc3364',
    ),
    'illuminate/console' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c794b268b9fd3c2a535c766c011fb574e51b071e',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '1641dda2d0750b68bb1264a3b37ff3973f2e6265',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '44f65d723b13823baa02ff69751a5948bde60c22',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e534676bac23bc17925f5c74c128f9c09b98f69',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '8168361548b2c5e2e501096cfbadb62a4a526290',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3bfaf6401742a9c6abca61b9b10e998e5b6449a',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0be3f3f79f8235ad7334919ca4094d5074e02f6',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3fc1d9deb1dc8c256c3394e1547dccdd386bb2f5',
    ),
    'illuminate/translation' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc8c7c94cf199077842829a79c2a02ac9146478e',
    ),
    'illuminate/validation' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '6201b88f7406c4b80fed2f859c40dc35a8391bfe',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v9.52.9',
      'version' => '9.52.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '0215165781b3269cdbecdfe63ffddd0e6cecfd6e',
    ),
    'laravel/ui' => 
    array (
      'pretty_version' => 'v4.2.2',
      'version' => '4.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a58ec468db4a340b33f3426c778784717a2c144b',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.67.0',
      'version' => '2.67.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1001b3bc75039b07f38a79db5237c4c529e04c8',
    ),
    'nunomaduro/termwind' => 
    array (
      'pretty_version' => 'v1.15.1',
      'version' => '1.15.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ab0b32c8caa4a2e09700ea32925441385e4a5dc',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1|2.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0|3.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '764e0b3939f5ca87cb904f570ef9be2d78a07865',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3ebc83d031b71c39da318ca8b7a07ecc67507ed',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cc9cac6586fc0c28cd173780ca696e419fefa11',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v6.0.20',
      'version' => '6.0.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e16b2676a4b3b1fa12378a20b29c364feda2a8d6',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7052547a0070cbeadd474e172b527a00d657301',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '511a08c03c1960e08a883f4cffcacd219b758354',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '639084e360537a19f9ee352433b84ce831f3d2da',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '2114fd60f26a296cc403a7939ab91478475a33d4',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd78d39c1599bd1188b8e26bb341da52c3c6d8a66',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9e72497367c23e08bf94176d2be45b00a9d232a',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c24b3fdbbe9fb2ef3a6afd8bbaadfd72dad681f',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
    ),
  ),
);
